#ifndef _ALEXT_H_
#define _ALEXT_H_

#define AL_FORMAT_MONO_IMA4                      0x1300
#define AL_FORMAT_STEREO_IMA4                    0x1301

#define AL_BLOCK_SIZE							0x1099

#endif	// #ifndef _ALEXT_H_