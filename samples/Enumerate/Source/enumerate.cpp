/*
 * Copyright (c) 2006, Creative Labs Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice, this list of conditions and
 * 	     the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 * 	     and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *     * Neither the name of Creative Labs Inc. nor the names of its contributors may be used to endorse or
 * 	     promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "aldlist.h"
#include "conio.h"

int main(void)
{
	ALDeviceList ald;
	int majorVersion, minorVersion;
	int i;

	printf("\n\nAll Available OpenAL Devices:\n");
	for (i = 0; i < ald.GetNumDevices(); i++) {
		ald.GetDeviceVersion(i, &majorVersion, &minorVersion);
		printf("%d. %s, Spec Version %d.%d\n", i + 1, ald.GetDeviceName(i), majorVersion, minorVersion);
	}

	printf("\nDefault device: %s\n", ald.GetDeviceName(ald.GetDefaultDevice()));
	
	printf("\nDevices with XRAM support:\n");
	ald.ResetFilters();
	ald.FilterDevicesExtension("EAX-RAM");
	i = ald.GetFirstFilteredDevice();
	if (i == ald.GetNumDevices()) {
		printf("None\n\n");
	} else {
		do {
			printf("%s\n", ald.GetDeviceName(i));
			i = ald.GetNextFilteredDevice();
		} while (i != ald.GetNumDevices());
	}
	printf("\n");

	printf("\nDevices with 1.1 API support:\n");
	ald.ResetFilters();
	ald.FilterDevicesMinVer(1, 1);
	i = ald.GetFirstFilteredDevice();
	if (i == ald.GetNumDevices()) {
		printf("None\n\n");
	} else {
		do {
			printf("%s\n", ald.GetDeviceName(i));
			i = ald.GetNextFilteredDevice();
		} while (i != ald.GetNumDevices());
	}
	printf("\n");

	printf("\nDevices with exclusive 1.0 API support:\n");
	ald.ResetFilters();
	ald.FilterDevicesMaxVer(1, 0);
	i = ald.GetFirstFilteredDevice();
	if (i == ald.GetNumDevices()) {
		printf("None\n\n");
	} else {
		do {
			printf("%s\n", ald.GetDeviceName(i));
			i = ald.GetNextFilteredDevice();
		} while (i != ald.GetNumDevices());
	}
	printf("\n");

	printf("Press a key to exit...\n");
	_getch();

	return 0;
}