#ifndef ISOUNDSYSTEM_H
#define ISOUNDSYSTEM_H

#include <string>
using namespace std;
//define buffer size for streaming media
//128kB buffer (cca 1.5 second), enaught for 1 fps updates
/*
OpenAL manual said:
	"Buffers containing audio data with more than one channel will be played without 3D
	spatialization features � these formats are normally used for background music."
//and it is true :)
*/
// until OpenAL is working on Wii
#ifdef WIN32
#define COMPILE_WITH_SOUND
#endif

#define STREAM_BUFFER_SIZE 1024*128 

enum ESoundSourceType
{
	SOUND_SOURCE_STREAMED,
	SOUND_SOURCE_BUFFERED
};

class ISoundListener
{
protected:
	ISoundListener() {};
	ISoundListener(ISoundListener&) {};
	virtual ~ISoundListener() {};
public:
	virtual void GetPosition(float* pos) = 0;
	virtual void SetPosition(float* pos) = 0;
	virtual void SetVelocity(float* vel) = 0;
	virtual void SetOrientation(float* dir) = 0;
	virtual void Release(void) = 0;
};

class ISoundSource
{
protected:
	ISoundSource() {};
	ISoundSource(ISoundSource&) {};
	virtual ~ISoundSource() {};
public:
	//rollof factor means (ref_distance+distance) in which sound has half gain
	//if 1 meter is 100 units	... rollof factor should be 1/100
	//							... reference distance should be 100
	virtual void SetRolloffFactor(float rf) = 0; 
	virtual void SetRefDistance(float rd) = 0; 
	virtual void SetPosition(float* pos) = 0;
	virtual void SetVelocity(float* vel) = 0;
	virtual void SetVolume(float vol) = 0;
	virtual float GetVolume(void) = 0;
	virtual void Play(bool loop=false) = 0;
	virtual void Play2D(bool loop=false) = 0;
	virtual void Stop(void) = 0;
	virtual void Pause(void) = 0;
	virtual void Release(void) = 0;
	virtual bool IsStopped(void) = 0;
	virtual bool IsPaused(void) = 0;
};
#define MAX_AUTO_DELETES 16

//declare sound manager
class ISoundSystem 
{
protected:
	ISoundSystem(void) {};
	~ISoundSystem(void) {};
public:
	// "u" units is ~ 1 meter in real world
	virtual void SetReferenceUnit(float u) = 0;

	// Initialization
	virtual bool Initialize(string &localizedPath) = 0;

	virtual bool HasAutodelete(ISoundSource* source, int id = 0) = 0;

	// After sound was played destroy it
	virtual void AutoDelete(ISoundSource* source, int id = 0) = 0;

	// Clear all autodeletes (after some part of game)
	virtual void ClearAutoDeletes(int id = 0) = 0;

	// get Listener interface
	virtual ISoundListener* GetListener(void) = 0;

	// Interface to load *.ogg or *.waw file  (3D)
	virtual bool CreateSoundSource(ISoundSource** our_source, const string& filename, bool streamed, bool localized = false) = 0;

	virtual void SetVolume(float volume, ESoundSourceType source_type) = 0;
	virtual float GetVolume(ESoundSourceType source_type) = 0;
	virtual void SetVolumeMute(bool mute, ESoundSourceType source_type) = 0;
	// Update (streams)
	virtual void Update(float dt) = 0;

	// Deinitialization
	virtual void Release(void) = 0;
};

#endif
