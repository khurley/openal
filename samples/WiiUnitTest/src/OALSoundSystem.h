#pragma once
#include "ISoundSystem.h"
#ifdef COMPILE_WITH_SOUND
#ifdef WIN32
#include <windows.h>
#endif
#include <vector>
#include <set>
#include <al.h>
#include <alc.h>
#include <ogg/ogg.h>
#include <vorbis/vorbisfile.h>
#include "SoundUtil.h"

//OpenAL32.lib ogg_static.lib vorbis_static.lib vorbisfile_static.lib
using namespace std;

class COALSoundListener :public ISoundListener
{
	COALSoundListener() {};
	COALSoundListener(COALSoundListener&) {};
	virtual ~COALSoundListener() {};
	friend class COALSoundSystem;
	float m_position[3];
public:
	void GetPosition(float* pos);
	void SetPosition(float* pos);
	void SetVelocity(float* vel);
	void SetOrientation(float* dir);
	void Release(void);
};

//declare sound source wrapper interface
class COALSoundSource :public ISoundSource
{
	friend class COALSoundSystem;
	unsigned int m_source;
	unsigned int m_buffer;
	bool m_2D;
	float volume;
public:
	COALSoundSource();
	void SetPosition(float* pos);
	void SetVelocity(float* vel);
	void SetVolume(float vol);
	float GetVolume(void);
	void SetRolloffFactor(float rf); //0 means 2D sound (no distance attenuation)
	void SetRefDistance(float rd);
	bool IsStopped(void);
	bool IsPaused(void);
	void Play(bool loop);
	void Play2D(bool loop);
	void Stop(void);
	void Pause(void);
	void Release(void);
	virtual ~COALSoundSource();
};

//declare streaming source wrapper interface
class COALStreamSource :public ISoundSource
{
protected:
	float volume;
	friend class COALSoundSystem;
	bool m_playing;
	bool m_2D;
	bool m_loop;
	unsigned int m_source; //source al_id
	unsigned int m_buffer[2]; //double buffered
	unsigned int m_frequency;
	unsigned short m_chanells, m_bitsampling;
	//must be overloaded
	virtual bool StreamNext(unsigned int id) = 0;
	COALStreamSource();
	void Update(void);/**/
public:
	void SetRefDistance(float rd); 
	void SetPosition(float* pos);
	void SetVelocity(float* vel);
	void SetVolume(float volume);
	float GetVolume(void);
	void SetRolloffFactor(float rf);
	bool IsStopped(void);
	bool IsPaused(void);
	void Play(bool loop);
	void Play2D(bool loop);
	virtual void Stop(void) = 0;
	void Pause(void);
	
	//must be overloaded
	virtual void Release(void) = 0;
	virtual ~COALStreamSource();
};
class COALStreamWAVSource: public COALStreamSource
{
	friend class COALSoundSystem;
	FILE* m_fp; //file stream
	unsigned int m_foffset;
	unsigned int m_fmaxoffset;
	bool StreamNext(unsigned int id);
public:
	COALStreamWAVSource();
	void Stop(void);
	void Release(void);
	~COALStreamWAVSource();
};
class COALStreamOGGSource: public COALStreamSource
{
	friend class COALSoundSystem;
	FILE* m_fp; //file stream
	OggVorbis_File m_ogg_file; //ogg file
	bool StreamNext(unsigned int id);
public:
	COALStreamOGGSource();
	void Stop(void);
	void Release(void);
	~COALStreamOGGSource();
};


/*
distance = max(distance,AL_REFERENCE_DISTANCE);
distance = min(distance,AL_MAX_DISTANCE);


//clamp distance ti range <AL_REFERENCE_DISTANCE, AL_MAX_DISTANCE>
//default <1.0f, INF>


                                  AL_REFERENCE_DISTANCE 
gain = ---------------------------------------------------------------------------------
		(AL_REFERENCE_DISTANCE +AL_ROLLOFF_FACTOR *(distance - AL_REFERENCE_DISTANCE))


//default is 

						1.0
gain = -------------------------------------------      //distance = <1.0f, INF>
		(1.0 +AL_ROLLOFF_FACTOR*(distance - 1.0))

REFERENCE_DISTANCE.......... attenuation start
MAX_DISTANCE................ attenuation end


			1.0
gain ~= ------------      //distance = <0.0f, INF>
		1.0 +1.0*d

if I need gain_at_500 units ~ half...... then rollof factor = 1/500.0

*/


//declare sound manager
class COALSoundSystem: public ISoundSystem
{
	float ref_unit;
	
	ISoundSource* mAutoDeletes[MAX_AUTO_DELETES];
	int mAutoDeletesID[MAX_AUTO_DELETES];
	//private move to globals
	ALCdevice *oal_device;
	ALCcontext *oal_context;
	COALSoundListener listener;
	string m_LocalizedPath;
public:
	
	COALSoundSystem(void);
	~COALSoundSystem(void);
public:
	void _RefreshVolume(ESoundSourceType source_type);
	void SetVolume(float volume, ESoundSourceType source_type);
	float GetVolume(ESoundSourceType source_type);
	void SetVolumeMute(bool mute, ESoundSourceType source_type);
	// After sound was played destroy it
	void AutoDelete(ISoundSource* source, int id);

	bool HasAutodelete(ISoundSource* source, int id);

	void SetReferenceUnit(float u);
	// Initialization
	bool Initialize(string &localizedPath);

	// Clear all autodeletes (after some part of game)
	void ClearAutoDeletes(int id);

	// get Listener interface
	COALSoundListener* GetListener(void);

	//common load sound
	bool CreateSoundSource(ISoundSource** our_source, const string& filename, bool streamed, bool localized = false);

	// Interface to load *.ogg or *.waw file (should be for <0.5MB uncompressed file) (3D)
	bool pCreateSoundSource(ISoundSource** our_source, const string& filename);

	// Interface to load *.ogg or *.wav streamed file (should be for >0.5MB uncompressed file) (3D)
	bool pCreateStreamedSource(ISoundSource** our_source, const string& filename);

	// Update (streams)
	void Update(float dt);

	// Deinitialization
	void Release(void);
};
#endif