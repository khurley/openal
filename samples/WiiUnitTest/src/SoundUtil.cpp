#include "SoundUtil.h"

CSoundUtil::CSoundUtil(void)
{
}

CSoundUtil::~CSoundUtil(void)
{
}

unsigned int  CSoundUtil::ReverseEndian32(unsigned int x)
{

	return(
            ((x >> 24) & 0x000000ff) |
            ((x >> 8)  & 0x0000ff00) |
            ((x << 8)  & 0x00ff0000) |
            ((x << 24) & 0xff000000) 
          );


} 
unsigned short CSoundUtil::ReverseEndian16(unsigned short data)
{

	return (((data & 0x00FF) << 8) | ((data & 0xFF00) >> 8));

}

unsigned char CSoundUtil::ReverseEndian8(unsigned char data)
{

	return (((data & 0x00FF) << 8));

}

void CSoundUtil::ReverseBuffer16(unsigned short *p, int samples)
{
    unsigned short *data;
    int count;

    data    = p;
    count   = samples;

    while (samples)
    {
        *data = ReverseEndian16(*data);

        data++;
        samples--;
    }
}

void CSoundUtil::ReverseBuffer8(unsigned char *p, int samples)
{
    unsigned char *data;
    int count;

    data    = p;
    count   = samples;

    while (samples)
    {
        *data = ReverseEndian8(*data);

        data++;
        samples--;
    }
}

