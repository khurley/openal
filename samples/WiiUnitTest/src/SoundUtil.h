#pragma once

class CSoundUtil
{
public:
	CSoundUtil(void);

	static unsigned char ReverseEndian8(unsigned char data);

	static unsigned short ReverseEndian16(unsigned short data);

	static unsigned int   ReverseEndian32(unsigned int x);

	static void ReverseBuffer16(unsigned short *p, int samples);

	static void ReverseBuffer8(unsigned char *p, int samples);

public:
	~CSoundUtil(void);
};
