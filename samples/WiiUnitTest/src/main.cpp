#include <file_io.h>
#include <console_io.h>
#include <revolution/vf.h>
#include <revolution/dvd.h>
#include <revolution/wpad.h>
#include "ISoundSystem.h"
#include "OALSoundSystem.h"

//intptr_t _findfirst(const char * _Filename, struct _finddata_t *_FindData)
//{
//	return ENOENT;
//}
//
//int _findnext(intptr_t _FindHandle, struct _finddata_t *_FindData)
//{
//	return ENOENT;
//}
//
//int _findclose(intptr_t _FindHandle)
//{
//	return 0;
//}

extern "C++" {
void InitVFSSystem( void );
void DeInitVFSSystem( void );
};

extern "C" {
	void  glutInit(int *argcp, char **argv);
};



//---------------------------------------------------------------------------------
int main(int argc, char **argv) 
{
	glutInit( &argc, argv );
	InitVFSSystem();


	ISoundSystem* mSoundSystem = new COALSoundSystem();
	ISoundSource* mSoundMenuBackground = 0;
	std::string localizedPath;

	mSoundSystem->Initialize(localizedPath);
	mSoundSystem->SetReferenceUnit(13.0f); //120 units ~ 1 meter ~ 2.4 segments
	
	if (mSoundSystem)
	{
		mSoundSystem->CreateSoundSource(&mSoundMenuBackground, "sounds/menu_bkg_music.ogg", true);
		if(mSoundMenuBackground) 
		{
			mSoundMenuBackground->Play(true);
		}
	}


	return 0;
}
