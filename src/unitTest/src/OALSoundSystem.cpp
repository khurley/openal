#include "OALSoundSystem.h"

extern "C++" {
	void SetIOFromDevice(int sourceType);
};

//#ifdef COMPILE_WITH_SOUND

#define OALSOUND_ENABLE_LOGGING

#ifdef OALSOUND_ENABLE_LOGGING
	FILE* flog=0;
#endif
float g_streams_volume=1.0f;
float g_sources_volume=1.0f;

float g_streams_mute_fac=1.0f;
float g_sources_mute_fac=1.0f;

float g_streams_mute_fac_act=1.0f;
float g_sources_mute_fac_act=1.0f;


void *g_oalstream_buffer=0;
set<COALStreamSource*> g_oalstreams;
set<COALSoundSource*> g_oalsources;

ALenum gFormats[2][3] = {	{AL_FORMAT_MONO_IMA4, AL_FORMAT_MONO8, AL_FORMAT_MONO16 },
							{AL_FORMAT_MONO_IMA4, AL_FORMAT_STEREO8, AL_FORMAT_STEREO16 }
						};

//declare listener wrapper interface
void COALSoundListener::SetPosition(float* pos)
{
	alListenerfv(AL_POSITION,pos);
	memcpy(m_position, pos, sizeof(float)*3);
}
void COALSoundListener::GetPosition(float* pos)
{
	memcpy(pos, m_position, sizeof(float)*3);
}
void COALSoundListener::SetVelocity(float* vel)
{
	alListenerfv(AL_VELOCITY,vel);
}
void COALSoundListener::SetOrientation(float* dir)
{
	alListenerfv(AL_ORIENTATION,dir);
	if(alGetInteger(AL_DISTANCE_MODEL)!=AL_INVERSE_DISTANCE_CLAMPED)
	{
		return;
	};
}
void COALSoundListener::Release(void)
{
}

//declare sound source wrapper interface
COALSoundSource::COALSoundSource()
{
	this->m_buffer=0;
	this->m_source=0;
	this->volume=1.0f;
}
void COALSoundSource::SetPosition(float* pos)
{
	alSourcefv(m_source,AL_POSITION,pos);
}
void COALSoundSource::SetVelocity(float* vel)
{
    alSourcefv(m_source, AL_VELOCITY, vel);
}
bool COALSoundSource::IsStopped(void)
{
	ALenum state;        alGetSourcei(m_source, AL_SOURCE_STATE, &state);
	if(state==AL_PLAYING)
	{
		return false;
	}
	else return true;
}
bool COALSoundSource::IsPaused(void)
{
	ALenum state;        alGetSourcei(m_source, AL_SOURCE_STATE, &state);
	if(state==AL_PAUSED)
	{
		return true;
	}
	else return true;
}
void COALSoundSource::Play(bool loop)
{
	alSourcei (m_source, AL_LOOPING,  loop? AL_TRUE: AL_FALSE);
	m_2D = false;
	alSourcePlay ( m_source );
}
void COALSoundSource::Play2D(bool loop)
{
	alSourcei (m_source, AL_LOOPING,  loop? AL_TRUE: AL_FALSE);
	alSourcef (m_source, AL_CONE_OUTER_GAIN, 1.0f);
	alSourcei(m_source, AL_SOURCE_RELATIVE, TRUE); 
	alSource3f(m_source, AL_POSITION, 0.0f, 0.0f, 0.0f); 

	m_2D = true;
	SetRolloffFactor(0.0f);
	alSourcePlay ( m_source );
}
void COALSoundSource::SetVolume(float volume)
{
	this->volume = volume;
	alSourcef(m_source, AL_GAIN, g_sources_volume*volume*g_sources_mute_fac_act);
}
float COALSoundSource::GetVolume(void)
{
	return volume;
}
void COALSoundSource::SetRolloffFactor(float rf)
{
	alSourcef(m_source, AL_ROLLOFF_FACTOR, rf);
}
void COALSoundSource::SetRefDistance(float rd)
{
	alSourcef(m_source, AL_REFERENCE_DISTANCE, rd);
}
void COALSoundSource::Pause(void)
{
	alSourcePause ( m_source );
}
void COALSoundSource::Stop(void)
{
	alSourceStop ( m_source );
}
void COALSoundSource::Release(void)
{
	if(m_source)
	{
		alSourceStop(m_source);    
		alDeleteSources(1, &m_source);
		m_source=0;
	}
	if(m_buffer)
	{
		alDeleteBuffers(1, &m_buffer);  
		this->m_buffer=0;
	}
	g_oalsources.erase(this);
	delete this;
}
COALSoundSource::~COALSoundSource()
{	
}

float COALSoundSource::GetOffset(ALenum eParam)
{
	float value;
	alGetSourcef(m_source, eParam, &value);
	return value;
}

//declare streaming source wrapper interface
COALStreamSource::COALStreamSource()
{
	this->volume=1.0f;
	this->m_buffer[0]=0;	this->m_buffer[1]=0;
	this->m_source=0;
	m_playing=false;
	m_loop=false;
}
void COALStreamSource::SetPosition(float* pos)
{
    alSourcefv(m_source, AL_POSITION, pos);
}
void COALStreamSource::SetVelocity(float* vel)
{
    alSourcefv(m_source, AL_VELOCITY, vel);
}
void COALStreamSource::SetRolloffFactor(float rf)
{
	alSourcef(m_source, AL_ROLLOFF_FACTOR, rf);
}
void COALStreamSource::SetRefDistance(float rd)
{
	alSourcef(m_source, AL_REFERENCE_DISTANCE, rd);
}
bool COALStreamSource::IsPaused(void)
{
	ALenum state;        alGetSourcei(m_source, AL_SOURCE_STATE, &state);
	if(state==AL_PAUSED)
	{
		return true;
	}
	else return true;
}
bool COALStreamSource::IsStopped(void)
{
	return !m_playing;
}
void COALStreamSource::Play(bool loop)
{
	alSourcei (m_source, AL_LOOPING,  loop? AL_TRUE: AL_FALSE);
	m_loop = loop;
	m_playing=true;
	alSourcePlay ( m_source );
	m_2D = false;
}
void COALStreamSource::Play2D(bool loop)
{
	alSourcei (m_source, AL_LOOPING,  loop? AL_TRUE: AL_FALSE);
	m_loop = loop;
	m_playing=true;
	alSourcePlay ( m_source );
	SetRolloffFactor(0.0f);
	alSourcei(m_source, AL_SOURCE_RELATIVE, TRUE); 
	alSource3f(m_source, AL_POSITION, 0.0f, 0.0f, 0.0f); 
	m_2D = true;
	
}

void COALStreamSource::SetVolume(float volume)
{
	this->volume=volume;
	alSourcef(m_source, AL_GAIN, g_streams_volume*volume*g_streams_mute_fac_act);
}
float COALStreamSource::GetVolume(void)
{
	return volume;
}
void COALStreamSource::Pause(void)
{
	m_playing=false;
	alSourcePause ( m_source );
}

void COALStreamSource::Update(void)
{
	ALenum state;        alGetSourcei(m_source, AL_SOURCE_STATE, &state);
	if(state!=AL_PLAYING && m_playing)
	{
		ALuint buffer;
		alSourceStop(m_source);  
		int queued;    
		alGetSourcei(m_source, AL_BUFFERS_QUEUED, &queued);        
		while(queued--)    
		{      
			//OSReport("%d",&queued);
			  alSourceUnqueueBuffers(m_source, 1, &buffer);
		}
		if(StreamNext(m_buffer[0]))   
		{		
			alSourceQueueBuffers(m_source, 1, &(m_buffer[0]));  
		}
		if(StreamNext(m_buffer[1]))   
		{		
			alSourceQueueBuffers(m_source, 1, &(m_buffer[1]));  
		}
		alSourcePlay(m_source);
		return;
	}
	else
	{
		int processed;    
		alGetSourcei(m_source, AL_BUFFERS_PROCESSED, &processed);
		while(processed--)    
		{   
			ALuint buffer;                
			alSourceUnqueueBuffers(m_source, 1, &buffer);                 
			if(StreamNext(buffer)) 
			{		
				alSourceQueueBuffers(m_source, 1, &buffer);  
			}
			else break;
		}
		return;
	}
}

COALStreamSource::~COALStreamSource()
{
}

float COALStreamSource::GetOffset(ALenum eParam)
{
	float value;
	alGetSourcef(m_source, eParam, &value);
	return value;
}


//wave stream source
COALStreamWAVSource::COALStreamWAVSource()
{
	m_fp=0;
	m_fmaxoffset=0;
	m_foffset=0;
}
void COALStreamWAVSource::Stop(void)
{
	m_playing=false;
	alSourceStop ( m_source );
	fseek(m_fp, m_foffset, SEEK_SET);
}
bool COALStreamWAVSource::StreamNext(unsigned int id)
{
	//m_fmaxoffset
	unsigned int size;
	size=(unsigned int)fread(g_oalstream_buffer, 1,STREAM_BUFFER_SIZE, m_fp);
	unsigned int fposition=ftell(m_fp);
	if(fposition>m_fmaxoffset)
	{
		if((fposition-m_fmaxoffset)>=size) size=0;
		else	size-=fposition-m_fmaxoffset;
	}
	if(size!=0)
	{

		alBufferData(id, gFormats[m_chanells-1][m_bitsampling >> 3], 
					g_oalstream_buffer, size, m_frequency);
		return true;
	}
	else
	{
		if(m_loop)	//play from begining
		{
			int t=ftell(m_fp);
			fseek(m_fp, m_foffset, SEEK_SET);
			if(t>ftell(m_fp)) //check if tracklen is not 0
			{
				size=(unsigned int)fread(g_oalstream_buffer, 1,STREAM_BUFFER_SIZE, m_fp);
				unsigned int fposition=ftell(m_fp);
				if(fposition>m_fmaxoffset)
				{
					if((fposition-m_fmaxoffset)>=size) size=0;
					else	size-=fposition-m_fmaxoffset;
				}
				if(size!=0)
				{
					ALenum format = gFormats[m_chanells-1][m_bitsampling >> 3];
					alBufferData(id, format, g_oalstream_buffer, size, m_frequency);
					return true;
				}
				else
				{
					fseek(m_fp, m_foffset, SEEK_SET);
					m_playing=false;
					return false;
				}
			}
			else 
			{
				fseek(m_fp, m_foffset, SEEK_SET);
				m_playing=false;
				return false;
			}
		}
		else //stop
		{
			fseek(m_fp, m_foffset, SEEK_SET);
			m_playing=false;
			return false;
		}
	}
}
void COALStreamWAVSource::Release(void)
{
	if(m_source)
	{
		alSourceStop(m_source);  
		int queued;       alGetSourcei(m_source, AL_BUFFERS_QUEUED, &queued);        
		while(queued--)    
		{	      
			ALuint buffer;  alSourceUnqueueBuffers(m_source, 1, &buffer);
		}
		alDeleteSources(1, &m_source);
		m_source=0;
	}
	if(m_buffer[0])
	{
		alDeleteBuffers(1, &(m_buffer[0]));
		m_buffer[0]=0;
	}
	if(m_buffer[1])
	{
		alDeleteBuffers(1, &(m_buffer[1]));
		m_buffer[1]=0;
	}
	if(m_fp)
	{
		fclose(m_fp);
		m_fp=0;
	}
	g_oalstreams.erase(this);
	delete this;
}
COALStreamWAVSource::~COALStreamWAVSource()
{
	
}


//wave stream source
COALStreamOGGSource::COALStreamOGGSource()
{
	m_fp=0;
}

void COALStreamOGGSource::Stop(void)
{
	m_playing=false;
	alSourceStop ( m_source );
	ov_time_seek(&m_ogg_file,0.0f);
}

bool COALStreamOGGSource::StreamNext(unsigned int id)
{
	int bitStream; 
	unsigned int size=0;
	char* buffer=(char*)g_oalstream_buffer;
	while (size < STREAM_BUFFER_SIZE)
	{
		unsigned int sz=0;
		// Read up to a buffer's worth of decoded sound data
		sz = ov_read(&m_ogg_file, &(buffer[size]), STREAM_BUFFER_SIZE-size, 0, 2, 1, &bitStream);
		if(sz==0) break;
		size+=sz;
		// Append to end of buffer
	}
	if(size!=0)
	{
		alBufferData(id, gFormats[m_chanells-1][m_bitsampling >> 3],
				g_oalstream_buffer, size, m_frequency);
		return true;
	}
	else
	{
		if(m_loop)
		{
			ov_time_seek(&m_ogg_file,0.0f);
			while (size < STREAM_BUFFER_SIZE)
			{
				unsigned int sz=0;
				// Read up to a buffer's worth of decoded sound data
				sz = ov_read(&m_ogg_file, &(buffer[size]), STREAM_BUFFER_SIZE-size, 0, 2, 1, &bitStream);
				if(sz==0) break;
				size+=sz;
				// Append to end of buffer
			}
			if(size!=0)
			{
				alBufferData(id, gFormats[m_chanells-1][m_bitsampling >> 3],
						g_oalstream_buffer, size, m_frequency);
				return true;
			}
			else
			{
				ov_time_seek(&m_ogg_file,0.0f);
				m_playing=false;
				return false;
			}
		}
		else
		{
			ov_time_seek(&m_ogg_file,0.0f);
			m_playing=false;
			return false;
		}
	}
}
void COALStreamOGGSource::Release(void)
{
	if(m_source)
	{
		alSourceStop(m_source);  
		int queued;       alGetSourcei(m_source, AL_BUFFERS_QUEUED, &queued);        
		while(queued--)    
		{	      
			ALuint buffer;  alSourceUnqueueBuffers(m_source, 1, &buffer);
		}
		
		alDeleteSources(1, &m_source);
		m_source=0;
	}
	if(m_buffer[0])
	{
		alDeleteBuffers(1, &(m_buffer[0]));
		m_buffer[0]=0;
	}
	if(m_buffer[1])
	{
		alDeleteBuffers(1, &(m_buffer[1]));
		m_buffer[1]=0;
	}
	if(m_fp)
	{
		ov_clear(&m_ogg_file);
		fclose(m_fp);
		m_fp=0;
	}
	g_oalstreams.erase(this);
	delete this;
}
COALStreamOGGSource::~COALStreamOGGSource()
{
	
}


//declare sound manager
COALSoundSystem::COALSoundSystem(void) 
{
	for(unsigned int i=0; i<MAX_AUTO_DELETES; i++)
	{
		mAutoDeletes[i]=0;
		mAutoDeletesID[i]=0;
	}
	ref_unit=1.0f;
	oal_device=0;
	
	//streams.clear();
	oal_context=0;

	m_LocalizedPath = "English/";
}
COALSoundSystem::~COALSoundSystem(void) 
{
	
}
// Initialization
bool COALSoundSystem::Initialize(string &localizedPath)
{
	m_LocalizedPath = localizedPath + "/";

#ifdef OALSOUND_ENABLE_LOGGING
	flog=fopen("oal_sound_system.log", "wt");
	
	
	
	{
		ALint iDeviceIndex = 0;
		const ALchar* pDeviceNames = NULL;
		const ALchar* pDefaultDevice = NULL;
	        
		OSReport("\n\nAll Available OpenAL Devices\n\n");

		pDeviceNames = alcGetString( NULL, ALC_DEVICE_SPECIFIER );
		pDefaultDevice = alcGetString( NULL, ALC_DEFAULT_DEVICE_SPECIFIER );

		if( !strlen(pDeviceNames) )
		{
			OSReport("None found.\n");
		}
		else
		{
			while( pDeviceNames && *pDeviceNames )
			{
				OSReport("%-2d - %-40s", iDeviceIndex, pDeviceNames );
				if( strlen(pDeviceNames) > 40 )
				{
					OSReport("\n%-45s", "" );
				}
				ALCdevice* pDevice = alcOpenDevice( pDeviceNames );

				if( pDevice )
				{
					ALint iMajorVersion, iMinorVersion;
					ALboolean bSpec10Support = AL_FALSE;
					ALboolean bSpec11Support = AL_FALSE;
					ALboolean bEFXSupport = AL_FALSE;

					alcGetIntegerv( pDevice, ALC_MAJOR_VERSION, sizeof(ALint), &iMajorVersion );
					alcGetIntegerv( pDevice, ALC_MINOR_VERSION, sizeof(ALint), &iMinorVersion );

					if ( (iMajorVersion == 1) && (iMinorVersion == 0) )
						bSpec10Support = AL_TRUE;
					else if ( (iMajorVersion > 1) || ((iMajorVersion == 1) && (iMinorVersion >= 1)) )
						bSpec11Support = AL_TRUE;

					if( alcIsExtensionPresent( pDevice, "ALC_EXT_EFX") )
					{
						bEFXSupport = AL_TRUE;
					}
					// Print default flag
					OSReport("pDeviceNames = %s",pDeviceNames);
					OSReport("   %-8c", !strcmp( pDeviceNames, pDefaultDevice ) ? 'X' : ' ' );
					// Print Spec version flags
					OSReport(" %-5c %-5c", bSpec10Support ? 'X' : ' ', bSpec11Support ? 'X' : ' ' );
					// Print XRAM ext support flag
					OSReport(" %-5c", bEFXSupport ? 'X' : ' ');
					alcCloseDevice( pDevice );
				}
				else
				{
					OSReport("ERROR: Doesn't open!\n");
				}
				OSReport("\n");
				iDeviceIndex++;
				pDeviceNames += strlen(pDeviceNames) + 1;
			}
		}
	    OSReport("\n\n");
	}
	
	OSReport( "Initialising open al ... ");
	fflush(flog);
#endif
	// Initialization
#ifdef RVL_SDK 
	oal_device = alcOpenDevice("Wii Audio Device"); // select the "preferred device"
#else
	oal_device = alcOpenDevice("Generic Software"); // select the "preferred device"
#endif
	if (oal_device) 
	{
		oal_context=alcCreateContext(oal_device,NULL);
		if(!oal_context)
		{
			alcCloseDevice(oal_device);
#ifdef OALSOUND_ENABLE_LOGGING
			OSReport( "Failed to create context\n");
			fflush(flog);
#endif
			return false;
		}
		if(!alcMakeContextCurrent(oal_context))
		{	
			alcDestroyContext(oal_context);
			alcCloseDevice(oal_device);
#ifdef OALSOUND_ENABLE_LOGGING
			OSReport( "Failed to make context current\n");
			fflush(flog);
#endif
			return false;
		}
	}
	else 
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "Failed to create device\n");
		fflush(flog);
#endif
		return false;
	}
	OSReport( "Informations:\n\tVendor: %s\n\tVersion: %s\n\tRenderer: %s\n",alGetString(AL_VENDOR), alGetString(AL_VERSION),alGetString(AL_RENDERER));
		
	//alDistanceModel(AL_LINEAR_DISTANCE);
	g_oalstream_buffer=new unsigned char[STREAM_BUFFER_SIZE];
	ALenum error;
	error=alGetError();
	if(AL_NO_ERROR==error) // clear error code
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "OK\n");
		fflush(flog);
#endif	
		return true;
	}
	else 
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "Failed with error:%d\n", error);
		fflush(flog);
#endif
		return false;
	}
}

void COALSoundSystem::SetVolume(float volume, ESoundSourceType source_type)
{
	if(source_type==SOUND_SOURCE_STREAMED)
	{
		g_streams_volume=volume;
		for(set<COALStreamSource*>::iterator i=g_oalstreams.begin(); i!=g_oalstreams.end(); i++)
		{
			(*i)->SetVolume((*i)->GetVolume());
		}
	}
	else if(source_type==SOUND_SOURCE_BUFFERED)
	{
		g_sources_volume=volume;
		for(set<COALSoundSource*>::iterator i=g_oalsources.begin(); i!=g_oalsources.end(); i++)
		{
			(*i)->SetVolume((*i)->GetVolume());
		}
	}
}
void COALSoundSystem::SetVolumeMute(bool mute, ESoundSourceType source_type)
{
	if(source_type==SOUND_SOURCE_STREAMED)
	{
		g_streams_mute_fac = mute ? 0.3f : 1.0f;
	}
	else if(source_type==SOUND_SOURCE_BUFFERED)
	{
		g_sources_mute_fac = mute ? 0.3f : 1.0f;
	}
}
void COALSoundSystem::_RefreshVolume(ESoundSourceType source_type)
{
	if(source_type==SOUND_SOURCE_STREAMED)
	{
		for(set<COALStreamSource*>::iterator i=g_oalstreams.begin(); i!=g_oalstreams.end(); i++)
		{
			(*i)->SetVolume((*i)->GetVolume());
		}
	}
	else if(source_type==SOUND_SOURCE_BUFFERED)
	{
		for(set<COALSoundSource*>::iterator i=g_oalsources.begin(); i!=g_oalsources.end(); i++)
		{
			(*i)->SetVolume((*i)->GetVolume());
		}
	}
}
float COALSoundSystem::GetVolume(ESoundSourceType source_type)
{
	if(source_type==SOUND_SOURCE_STREAMED)
	{
		return g_streams_volume;
	}
	else if(source_type==SOUND_SOURCE_BUFFERED)
	{
		return g_sources_volume;
	}
	else return 0.0f;
}
void COALSoundSystem::ClearAutoDeletes(int id)
{
	for(unsigned int i=0; i<MAX_AUTO_DELETES; i++)
	{
		if(mAutoDeletes[i]) 
		{
			if(id == 0 || id == mAutoDeletesID[i])
			{
				mAutoDeletes[i]->Stop();
				mAutoDeletes[i]->Release();
				mAutoDeletes[i]=0;
				mAutoDeletesID[i] = 0;
			}
		}
	}
}
bool COALSoundSystem::HasAutodelete(ISoundSource* source, int id)
{
	for(unsigned int i=0; i<MAX_AUTO_DELETES; i++)
	{
		if(mAutoDeletes[i]) 
		{
			if(source == mAutoDeletes[i])
			{
				if(id ==0 || id == mAutoDeletesID[i])
					return true;
			}
		}
	}
	return false;
}
// After sound was played destroy it
void COALSoundSystem::AutoDelete(ISoundSource* source, int id)
{	
	if(id == 0) // effect
	{
		for(unsigned int i=0; i< 6; i++)
		{
			if(!mAutoDeletes[i]) 
			{
				mAutoDeletes[i]=source;
				mAutoDeletesID[i]=id;
				return;
			}
			else
			{
			}
		}
	}
	else // voice
	{
		for(unsigned int i=6; i<MAX_AUTO_DELETES; i++)
		{
			if(!mAutoDeletes[i]) 
			{
				mAutoDeletes[i]=source;
				mAutoDeletesID[i]=id;
				return;
			}
			else
			{
			}
		}
	}
	//empty slot not found, delete immediatelly
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "Not enaught autodelete slots. Deleting immediatelly");
#endif
	source->Release();
}
void COALSoundSystem::SetReferenceUnit(float u)
{
	ref_unit=u;
}
bool COALSoundSystem::CreateSoundSource(ISoundSource** our_source, const string& filename, bool streamed, bool localized)
{
	
	string localizedFilename;

	if (localized)
	{
		localizedFilename = m_LocalizedPath + filename;
	}
	else
	{
		localizedFilename = filename;
	}
	if(streamed)
	{
		if(this->pCreateStreamedSource(our_source, localizedFilename))
		{
			(*our_source)->SetRefDistance(ref_unit);
			(*our_source)->SetRolloffFactor(1.0f/ref_unit);
			(*our_source)->SetVolume((*our_source)->GetVolume());
			return true;
		}
	}
	else
	{
		if(this->pCreateSoundSource(our_source, localizedFilename))
		{
			(*our_source)->SetRefDistance(ref_unit);
			(*our_source)->SetRolloffFactor(1.0f/ref_unit);
			(*our_source)->SetVolume((*our_source)->GetVolume());
			return true;
		}
	}
	return false;
}
// Interface to load *.ogg or *.wav file (3D) not streamed
bool COALSoundSystem::pCreateSoundSource(ISoundSource** our_source, const string& filename)
{
	unsigned short block_align = 0;
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "Loading not streamed sound \"%s\" ... ", filename.c_str());
	fflush(flog);
#endif
	unsigned int buffer, source;
	unsigned int size, frequency;
	unsigned short chanells, bitsampling;
	unsigned char *data;
	//determine format by file extension
	unsigned int i=(unsigned int)filename.find_last_of('.');
	string ext=filename.substr(i, filename.size());
	// load file
	FILE *fp=fopen(filename.c_str(), "rb");	
	if(!fp) 
	{
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "open file FAIL\n");
	fflush(flog);
#endif
		return false; //open file for bin read
	}
	//determine file 
	if(ext==".ogg" || ext==".vorbis")
	{
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "as ogg vorbis ... ");
	fflush(flog);
#endif
		// OGG FILE START
		unsigned int bfr_sz=1024*64;//64k bytes buffer
		char *bfr=new char[bfr_sz];
		vorbis_info *pInfo;				    OggVorbis_File oggFile;
		ov_open(fp, &oggFile, NULL, 0);	    pInfo = ov_info(&oggFile, -1);
		chanells=pInfo->channels;			frequency = pInfo->rate;
		if(chanells!=1 && chanells!=2)		return false; 
		bitsampling=16;	//allways for ogg files 
		int bitStream; long bytes; vector<char> out_buffer;
		do {
			// Read up to a buffer's worth of decoded sound data
			bytes = ov_read(&oggFile, bfr, bfr_sz, 0, 2, 1, &bitStream);
			// Append to end of buffer
			out_buffer.insert(out_buffer.end(), bfr, bfr + bytes);
		} while (bytes > 0);
		ov_clear(&oggFile);
		size=(unsigned int)out_buffer.size();
		data= new unsigned char[size]; memcpy(data,&(out_buffer[0]), size);
		


		out_buffer.clear(); delete[] bfr;
		// OGG END
	}	else
	if(ext==".wav" || ext==".waw" || ext==".wave" || ext==".wawe")
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "as wave ... ");
		fflush(flog);
#endif
		// WAV FILE START
		char bfr[16]={0};	fread(bfr,1,12,fp);  //read pre header
		if(memcmp(bfr,"RIFF",4) || memcmp(&(bfr[8]),"WAVE",4)) return false; //not a wave file
		// format block size (32bit) must be 16, format tag (16bit) must be 1
		// skip all non-"fmt " chunks
		fread(bfr,1,4,fp);
		while(memcmp(bfr, "fmt ",4))
		{
			unsigned int toskip=0;
			fread(&toskip,4,1,fp);
			fseek(fp, toskip,SEEK_CUR);
#ifdef RVL_SDK
			toskip = CSoundUtil::ReverseEndian32(toskip);// ENDIAN to wii
#endif
			fread(bfr,1,4,fp);
			if(feof(fp)) return false;
		}
		unsigned short fmt; 
		fread(&size,4,1,fp);
#ifdef RVL_SDK
		size = CSoundUtil::ReverseEndian32(size);// ENDIAN to wii
#endif
		fread(&fmt,2,1,fp);
#ifdef RVL_SDK
		fmt = CSoundUtil::ReverseEndian16(fmt);// ENDIAN to wii
#endif
		if ((fmt!=1) && (fmt != 17))
			return false; // invalid format
		//mono/stereo
		fread(&chanells, 2,1,fp); 
#ifdef RVL_SDK
		chanells = CSoundUtil::ReverseEndian16(chanells);// ENDIAN to wii
#endif
		if(chanells!=1 && chanells!=2) return false;

		//bitrate & bit sampling
		fread(&frequency, 4,1,fp);	fseek(fp,4/**/,SEEK_CUR);
#ifdef RVL_SDK
		frequency = CSoundUtil::ReverseEndian32(frequency);// ENDIAN to wii
#endif
		fread(&block_align,2,1,fp); 
#ifdef RVL_SDK
		block_align = CSoundUtil::ReverseEndian16(block_align);// ENDIAN to wii
#endif
		fread(&bitsampling,2,1,fp); 
#ifdef RVL_SDK
		bitsampling = CSoundUtil::ReverseEndian16(bitsampling);// ENDIAN to wii
#endif
		fseek(fp,size-16, SEEK_CUR);
		if (bitsampling!=8 && bitsampling!=16 && bitsampling != 4)
			return false; 
		//skip all nondata chunks
		fread(bfr,1,4,fp);
		while(memcmp(bfr, "data",4))
		{
			unsigned int toskip=0;
			fread(&toskip,4,1,fp);
#ifdef RVL_SDK
			toskip = CSoundUtil::ReverseEndian32(toskip);// ENDIAN to wii
#endif
			fseek(fp, toskip,SEEK_CUR);
			fread(bfr,1,4,fp);
			if(feof(fp)) 
				return false;
		}
		fread(&size,4,1,fp);
#ifdef RVL_SDK
		size = CSoundUtil::ReverseEndian32(size);// ENDIAN to wii
#endif
		
		//data size & data alloc
		data=new unsigned char[size];
		if(fread(data, 1,size, fp)!= size) { fclose(fp);	return false; }
	}
	else 
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "unknown format\n");
		fflush(flog);
#endif
		return false;
	}
	fclose(fp);
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "OK\n");
		OSReport( "Creating sound source with: chanells: %d, bitsampling: %d, frequency: %d, size: %d ... ",chanells,bitsampling,frequency, size );
		fflush(flog);
#endif
	ALenum err=alGetError();
		//create buffer and source
	alGenBuffers(1, &buffer);
	alGenSources(1, &source);

	//SetIOFromDevice(2);
#ifdef RVL_SDK
	alBufferi(buffer, AL_BLOCK_SIZE, block_align);
#endif

	//FILE *myFile = fopen("/tmp/test.raw", "wb+");

	alBufferData(		buffer, gFormats[chanells-1][bitsampling >> 3],
						data, size, frequency);
	//fwrite(data, 1, size, myFile);
	//fclose(myFile);
	//SetIOFromDevice(1);
	delete[] data;
	//bind buffer to source
	alSourcei(source,AL_BUFFER, buffer);
	err=alGetError();
	if(AL_NO_ERROR==err) // clear error code
	{
		COALSoundSource* ss=new COALSoundSource();
		ss->m_source=source;
		ss->m_buffer=buffer;
		g_oalsources.insert(ss);
		(*our_source)=ss;
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "OK\n");
		fflush(flog);
#endif
		return true;
	}
	else 
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "Failed\n");
		fflush(flog);
#endif
		return false;
	}
}
// Interface to load *.ogg or *.wav streamed source (3D)
bool COALSoundSystem::pCreateStreamedSource(ISoundSource** our_source, const string& filename)
{
	unsigned short block_align = 0;
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "Loading streamed sound \"%s\" ... ", filename.c_str());
	fflush(flog);
#endif
	unsigned int size;
	unsigned int frequency;
	unsigned short chanells, bitsampling;
	//determine format by file extension
	unsigned int i=(unsigned int)filename.find_last_of('.');
	string ext=filename.substr(i, filename.size());
	COALStreamSource* ss=0;
	// load file
	FILE *fp=fopen(filename.c_str(), "rb");
	if(!fp)
	{
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "open file FAIL\n");
	fflush(flog);
#endif
		return false; //open file for bin read
	}
	//solve wav file
	if(ext==".wav" || ext==".waw" || ext==".wave" || ext==".wawe")
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "as wave ... ");
		fflush(flog);
#endif
		// WAV FILE START
		char bfr[16]={0};	fread(bfr,1,12,fp);  //read pre header
		if(memcmp(bfr,"RIFF",4) || memcmp(&(bfr[8]),"WAVE",4)) return false; //not a wave file
		// format block size (32bit) must be 16, format tag (16bit) must be 1
		// skip all non-"fmt " chunks
		fread(bfr,1,4,fp);
		while(memcmp(bfr, "fmt ",4))
		{
			unsigned int toskip=0;
			fread(&toskip,4,1,fp);
#ifdef RVL_SDK
			toskip = CSoundUtil::ReverseEndian32(toskip);// ENDIAN to wii
#endif
			fseek(fp, toskip,SEEK_CUR);
			fread(bfr,1,4,fp);
			if(feof(fp)) return false;
		}
		unsigned short fmt; 
		fread(&size,4,1,fp);
		#ifdef RVL_SDK
		size = CSoundUtil::ReverseEndian32(size);// ENDIAN to wii
#endif
		fread(&fmt,2,1,fp);
#ifdef RVL_SDK
		fmt = CSoundUtil::ReverseEndian16(fmt);// ENDIAN to wii
#endif
		if ((fmt!=1) && (fmt != 17))
			return false; // invalid format
		//mono/stereo
		fread(&chanells, 2,1,fp); 
#ifdef RVL_SDK
		chanells = CSoundUtil::ReverseEndian16(chanells);// ENDIAN to wii
#endif
		if(chanells!=1 && chanells!=2)
			return false;

		//bitrate & bit sampling
		fread(&frequency, 4,1,fp);	fseek(fp,4/**/,SEEK_CUR);
#ifdef RVL_SDK
		frequency = CSoundUtil::ReverseEndian32(frequency);// ENDIAN to wii
#endif
		fread(&block_align,2,1,fp); 
#ifdef RVL_SDK
		block_align = CSoundUtil::ReverseEndian16(block_align);// ENDIAN to wii
#endif
		fread(&bitsampling,2,1,fp); 
#ifdef RVL_SDK
		bitsampling = CSoundUtil::ReverseEndian16(bitsampling);// ENDIAN to wii
#endif
		fseek(fp,size-16, SEEK_CUR);
		if ((bitsampling!=8 && bitsampling!=16)	&& bitsampling != 4)
			return false; 
		//skip all nondata chunks
		fread(bfr,1,4,fp);
		while(memcmp(bfr, "data",4))
		{
			unsigned int toskip=0;
			fread(&toskip,4,1,fp);
#ifdef RVL_SDK
			toskip = CSoundUtil::ReverseEndian32(toskip);// ENDIAN to wii
#endif
			fseek(fp, toskip,SEEK_CUR);
			fread(bfr,1,4,fp);
			if(feof(fp)) return false;
		}
		unsigned int datasize=0; 
		fread(&datasize,4,1,fp);
#ifdef RVL_SDK
		datasize = CSoundUtil::ReverseEndian32(datasize);// ENDIAN to wii
#endif
		
		COALStreamWAVSource* swav=new COALStreamWAVSource();		
		swav->m_fp=fp;
		swav->m_foffset=ftell(fp);
		swav->m_fmaxoffset=swav->m_foffset+datasize;
		ss=swav;		(*our_source)=ss;
	//WAV END
	}	else 
	if(ext==".ogg" || ext==".vorbis")
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "as ogg vorbis ... ");
		fflush(flog);
#endif
		// OGG FILE START
		vorbis_info *pInfo;				    OggVorbis_File oggFile;
		ov_open(fp, &oggFile, NULL, 0);	    pInfo = ov_info(&oggFile, -1);
		chanells=pInfo->channels;			frequency = pInfo->rate;
		if(chanells!=1 && chanells!=2)		return false; 
		bitsampling=16;	//allways for ogg files 
		//create stream source
		COALStreamOGGSource* sogg=new COALStreamOGGSource();		sogg->m_fp=fp;
		sogg->m_ogg_file=oggFile;
		ss=sogg;		(*our_source)=ss;
		// OGG END
	}
	else 
	{
#ifdef OALSOUND_ENABLE_LOGGING
		OSReport( "unknown format\n");
		fflush(flog);
#endif
		return false;
	}
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "OK\n");
	OSReport( "Creating sound source with: chanells: %d, bitsampling: %d, frequency: %d, size: %d ... ",chanells,bitsampling,frequency, size );
	fflush(flog);
#endif
	//common part
	ss->m_bitsampling=bitsampling;
	ss->m_chanells=chanells; ss->m_frequency=frequency;
	//start reading sound data
	alGenBuffers(2, ss->m_buffer);
	alGenSources(1, &(ss->m_source));//data size & data alloc
#ifdef RVL_SDK
	alBufferi(ss->m_buffer[0], AL_BLOCK_SIZE, block_align);
	alBufferi(ss->m_buffer[1], AL_BLOCK_SIZE, block_align);
#endif

	//read buffer #0
	ss->StreamNext(ss->m_buffer[0]);
	alSourceQueueBuffers(ss->m_source, 1, &(ss->m_buffer[0]));
	//read buffer #1
	ss->StreamNext(ss->m_buffer[1]);
	alSourceQueueBuffers(ss->m_source, 1, &(ss->m_buffer[1]));
	g_oalstreams.insert(ss);
	//streams.insert(ss);
#ifdef OALSOUND_ENABLE_LOGGING
	OSReport( "OK\n");
	fflush(flog);
#endif
	return true;
}

// Get listener interface
COALSoundListener* COALSoundSystem::GetListener(void)
{
	return &listener;
}

void COALSoundSystem::Update(float dt)
{
	for(unsigned int i=0; i<MAX_AUTO_DELETES; i++)
	{
		if(mAutoDeletes[i]) 
		{
			if(mAutoDeletes[i]->IsStopped())
			{	
				mAutoDeletes[i]->Release();
				mAutoDeletes[i]=0;
			}
		}
	}
	for(set<COALStreamSource*>::iterator i=g_oalstreams.begin(); i!=g_oalstreams.end(); i++)
		(*i)->Update();

	float fadespeed = 0.5f; // fade from 0 to 1 in 2 seconds

	if(g_streams_mute_fac > g_streams_mute_fac_act)
	{
		g_streams_mute_fac_act += fadespeed*dt;
		if(g_streams_mute_fac_act > g_streams_mute_fac)
			g_streams_mute_fac_act = g_streams_mute_fac;
		// refresh 
		_RefreshVolume(SOUND_SOURCE_STREAMED);
	}
	else if(g_streams_mute_fac < g_streams_mute_fac_act)
	{
		g_streams_mute_fac_act -= fadespeed*dt;
		if(g_streams_mute_fac_act < g_streams_mute_fac)
			g_streams_mute_fac_act = g_streams_mute_fac;
		// refresh
		_RefreshVolume(SOUND_SOURCE_STREAMED);
	}

	if(g_sources_mute_fac > g_sources_mute_fac_act)
	{
		g_sources_mute_fac_act += fadespeed*dt;
		if(g_sources_mute_fac_act > g_sources_mute_fac)
			g_sources_mute_fac_act = g_sources_mute_fac;
		// refresh
		_RefreshVolume(SOUND_SOURCE_BUFFERED);
	}
	else if(g_sources_mute_fac < g_sources_mute_fac_act)
	{
		g_sources_mute_fac_act -= fadespeed*dt;
		if(g_sources_mute_fac_act < g_sources_mute_fac)
			g_sources_mute_fac_act = g_sources_mute_fac;
		// refresh
		_RefreshVolume(SOUND_SOURCE_BUFFERED);
	}
}

void COALSoundSystem::Release(void)
{
#ifdef OALSOUND_ENABLE_LOGGING
	if(flog)
	{
		OSReport( "Releasing open al ... ");
		fflush(flog);
	}
#endif
	ClearAutoDeletes(0);
	if(g_oalstream_buffer) delete[] (char*)g_oalstream_buffer;
	g_oalstream_buffer=0;
	alcMakeContextCurrent(NULL);
	if(oal_context)
		alcDestroyContext(oal_context);
	if(oal_device)
		alcCloseDevice(oal_device);
	oal_context=0;
	oal_device=0;
	delete this;
#ifdef OALSOUND_ENABLE_LOGGING
	if(flog)
	{
		OSReport( "OK\n");
		fclose(flog);
	}
#endif
}

//#endif