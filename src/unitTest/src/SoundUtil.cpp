#include "SoundUtil.h"
#include <revolution/os.h>
#include <string>
#include <vector>
#include <ogg/ogg.h>
#include <vorbis/vorbisfile.h>
using namespace std;

extern "C++" {
void SetIOFromDevice(int sourceType);
};

CSoundUtil::CSoundUtil(void)
{
}

CSoundUtil::~CSoundUtil(void)
{
}

unsigned int  CSoundUtil::ReverseEndian32(unsigned int x)
{

	return(
            ((x >> 24) & 0x000000ff) |
            ((x >> 8)  & 0x0000ff00) |
            ((x << 8)  & 0x00ff0000) |
            ((x << 24) & 0xff000000) 
          );


} 
unsigned short CSoundUtil::ReverseEndian16(unsigned short data)
{

	return (((data & 0x00FF) << 8) | ((data & 0xFF00) >> 8));

}

unsigned char CSoundUtil::ReverseEndian8(unsigned char data)
{

	return (((data & 0x00FF) << 8));

}

void CSoundUtil::ReverseBuffer16(void *p, int samples)
{
    unsigned short *data = (unsigned short*)p;

    int count;
    count   = samples;

    while (count)
    {
        *data = ReverseEndian16(*data);

        data++;
        count--;
    }
}

void CSoundUtil::ReverseBuffer8(unsigned char *p, int samples)
{
    unsigned char *data;
    int count;

    data    = p;
    count   = samples;

    while (samples)
    {
        *data = ReverseEndian8(*data);

        data++;
        samples--;
    }
}


// Interface to load *.ogg or *.wav file (3D) not streamed
unsigned char* CSoundUtil::pCreateSoundSource(char* filename, long* _size)
{
	unsigned int buffer, source;
	unsigned int size, frequency;
	unsigned short chanells, bitsampling;
	unsigned char *data;

	OSReport( "Loading not streamed sound \"%s\" ... ", filename);

	//determine format by file extension
	string sfilename(filename);

	unsigned int i=(unsigned int)sfilename.find_last_of('.');
	string ext = sfilename.substr(i, sfilename.size());

	// load file
	FILE *fp=fopen(filename, "rb");	
	if(!fp) 
	{
		OSReport( "open file FAIL\n");
		return NULL; //open file for bin read
	}
	//determine file 
	if(ext==".ogg" || ext==".vorbis")
	{
		OSReport( "as ogg vorbis ... ");
		// OGG FILE START
		unsigned int bfr_sz=1024*64;//64k bytes buffer
		char *bfr=new char[bfr_sz];
		vorbis_info *pInfo;				    OggVorbis_File oggFile;
		ov_open(fp, &oggFile, NULL, 0);	    pInfo = ov_info(&oggFile, -1);
		chanells=pInfo->channels;			frequency = pInfo->rate;
		if(chanells!=1 && chanells!=2)		return false; 
		bitsampling=16;	//allways for ogg files 
		int bitStream; long bytes; vector<char> out_buffer;
		do {
			// Read up to a buffer's worth of decoded sound data
			bytes = ov_read(&oggFile, bfr, bfr_sz, 0, 2, 1, &bitStream);
			// Append to end of buffer
			out_buffer.insert(out_buffer.end(), bfr, bfr + bytes);
		} while (bytes > 0);
		ov_clear(&oggFile);
		size=(unsigned int)out_buffer.size();
		_size[0] = (long)size;
		data= new unsigned char[size]; memcpy(data,&(out_buffer[0]), size);
			out_buffer.clear(); delete[] bfr;
		// OGG END
	}
	else if(ext==".wav" || ext==".waw" || ext==".wave" || ext==".wawe")
	{
		OSReport( "as wave ... ");

		// WAV FILE START
		char bfr[16]={0};	fread(bfr,1,12,fp);  //read pre header
		if(memcmp(bfr,"RIFF",4) || memcmp(&(bfr[8]),"WAVE",4)) return false; //not a wave file
		// format block size (32bit) must be 16, format tag (16bit) must be 1
		// skip all non-"fmt " chunks
		fread(bfr,1,4,fp);
		while(memcmp(bfr, "fmt ",4))
		{
			unsigned int toskip=0;
			fread(&toskip,4,1,fp);
#ifdef RVL_SDK
			toskip = CSoundUtil::ReverseEndian32(toskip);// ENDIAN to wii
#endif
			fseek(fp, toskip,SEEK_CUR);
			fread(bfr,1,4,fp);
			if(feof(fp)) return false;
		}
		unsigned short fmt; 
		fread(&size,4,1,fp);
		size = CSoundUtil::ReverseEndian32(size);// ENDIAN to wii
		fread(&fmt,2,1,fp);
		fmt = CSoundUtil::ReverseEndian16(fmt);// ENDIAN to wii
		if ((fmt!=1) && (fmt != 17))
			return false; // invalid format

		//mono/stereo
		fread(&chanells, 2,1,fp); 
		chanells = CSoundUtil::ReverseEndian16(chanells);// ENDIAN to wii
		if(chanells!=1 && chanells!=2) return false;

		//bitrate & bit sampling
		fread(&frequency, 4,1,fp);	fseek(fp,4/**/,SEEK_CUR);
		frequency = CSoundUtil::ReverseEndian32(frequency);// ENDIAN to wii
		unsigned short block_align;
		fread(&block_align,2,1,fp); 

		block_align = CSoundUtil::ReverseEndian16(block_align);// ENDIAN to wii
		fread(&bitsampling,2,1,fp); 
		bitsampling = CSoundUtil::ReverseEndian16(bitsampling);// ENDIAN to wii

		fseek(fp,size-16, SEEK_CUR);
		if(bitsampling!=8 && bitsampling!=16 && bitsampling != 4)
			return false; 
		
		//skip all nondata chunks
		fread(bfr,1,4,fp);
		while(memcmp(bfr, "data",4))
		{
			unsigned int toskip=0;
			fread(&toskip,4,1,fp);
#ifdef RVL_SDK
			toskip = CSoundUtil::ReverseEndian32(toskip);// ENDIAN to wii
#endif
			fseek(fp, toskip,SEEK_CUR);
			fread(bfr,1,4,fp);
			if(feof(fp)) return false;
		}
		fread(&size,4,1,fp);
		size = CSoundUtil::ReverseEndian32(size);// ENDIAN to wii
		
		//data size & data alloc
		data=new unsigned char[size];
		_size[0] = (long)size;
		if( fread(data, 1,size, fp)!= size) 
		{ 
			fclose(fp);	
			return NULL; 
		}
	}
	else 
	{
		OSReport( "unknown format\n");
		return NULL;
	}
	fclose(fp);
	OSReport( "OK\n");
	OSReport( "Creating sound source with: chanells: %d, bitsampling: %d, frequency: %d ... ",chanells,bitsampling,frequency );

	u32 num_samp = (size << 3) /  bitsampling;
	OSReport("num_samp=%d", num_samp);
	//_size[0] = num_samp;

	SetIOFromDevice(2);
	FILE *myFile = fopen("/tmp/test8.raw", "wb+");
	fwrite(data, 1, size, myFile);
	fclose(myFile);
	SetIOFromDevice(1);

	if(bitsampling==16)
	{
		OSReport("deveria passar aqui? if(bitsampling==16)");
		CSoundUtil::ReverseBuffer16((unsigned short*)data, num_samp);//Endian!!!!!!!!!!!!!!!
	}
	if(bitsampling==8)
	{
		OSReport("deveria passar aqui? if(bitsampling==8)");
		CSoundUtil::ReverseBuffer8((unsigned char*)data, num_samp);//Endian!!!!!!!!!!!!!!!
	}

	SetIOFromDevice(2);
	FILE *myFile2 = fopen("/tmp/test8_2.raw", "wb+");
	fwrite(data, 1, size, myFile2);
	fclose(myFile2);
	SetIOFromDevice(1);

	
	return data;
}

