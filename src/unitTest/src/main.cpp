#include "gl/glut.h"
#include "ISoundSystem.h"
#include "OALSoundSystem.h"


extern "C++" {
void InitVFSSystem( void );
void DeInitVFSSystem( void );
};

ISoundSystem* g_SoundSystem = new COALSoundSystem();
ISoundSource* g_SoundMenuBackground = 0;


void DisplayImage(int t)
{

}

void Draw()
{
	g_SoundSystem->Update(.1);
}

// Function callback - Draw the cube
void KeyFunc(unsigned char key, int x, int y)
{
	float volume = g_SoundSystem->GetVolume(SOUND_SOURCE_STREAMED);

	if (key == 'a')
	{
		volume += 0.1;
	}
	else if (key == 'b')
	{
		volume -= 0.1;
	}
	if (volume > 1.0)
	{
		volume = 1.0;
	}
	else if (volume < 0.0)
	{
		volume = 0.0;
	}
	OSReport("volume=%f\n", volume);
	g_SoundSystem->SetVolume(volume, SOUND_SOURCE_STREAMED);

	if (g_SoundMenuBackground)
	{
		OSReport("AL_SEC_OFFSET = %f\n", g_SoundMenuBackground-> GetOffset(AL_SEC_OFFSET));
		OSReport("AL_SAMPLE_OFFSET = %f\n", g_SoundMenuBackground-> GetOffset(AL_SAMPLE_OFFSET));
		OSReport("AL_BYTE_OFFSET = %f\n", g_SoundMenuBackground-> GetOffset(AL_BYTE_OFFSET));
	}
}


//---------------------------------------------------------------------------------
int main(int argc, char **argv) 
{
	glutInit( &argc, argv );
	InitVFSSystem();

	glutDisplayFunc(Draw);
	glutKeyboardFunc(KeyFunc);


	std::string localizedPath;

	g_SoundSystem->Initialize(localizedPath);
	g_SoundSystem->SetReferenceUnit(13.0f); //120 units ~ 1 meter ~ 2.4 segments
	
	alEnable(AL_LITTLE_ENDIAN);

	if (g_SoundSystem)
	{
		g_SoundSystem->CreateSoundSource(&g_SoundMenuBackground, "English/R1RSC/r1ca1spch/10185.wav", true);
		//g_SoundSystem->CreateSoundSource(&g_SoundMenuBackground, "sounds/music/fantasyland_60_loop.wav", false);
		
		if(g_SoundMenuBackground) 
		{
			g_SoundMenuBackground->Play(true);
		}
	}
	glutMainLoop();

	return 0;
}